from invoke import task

from tasks.common import CONFIG_URI
from tasks.common import ENV
from tasks.common import load_environ
from tasks.common import print_title


@task()
def flake8(c, config_uri=CONFIG_URI, env=ENV):
    """ Running flake8 tests """

    print_title(flake8.__doc__)

    load_environ(env)

    c.run('flake8 pyramid_helpers tests', pty=True)


@task()
def functional(c, config_uri=CONFIG_URI, env=ENV, test='tests'):
    """ Running functional tests """

    print_title(functional.__doc__)

    load_environ(env)

    c.run(f'pytest --config-uri={config_uri} --disable-warnings --verbose {test}', pty=True)


@task()
def pylint(c, config_uri=CONFIG_URI, env=ENV):
    """ Running pylint tests """

    print_title(pylint.__doc__)

    load_environ(env)

    c.run('pylint pyramid_helpers tests', pty=True)


@task()
def static(c, config_uri=CONFIG_URI, env=ENV):
    flake8(c, config_uri=config_uri, env=env)
    pylint(c, config_uri=config_uri, env=env)


@task(default=True)
def all(c, config_uri=CONFIG_URI, env=ENV):
    flake8(c, config_uri=config_uri, env=env)
    functional(c, config_uri=config_uri, env=env)
    pylint(c, config_uri=config_uri, env=env)
