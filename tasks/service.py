from invoke import task

from tasks.common import CONFIG_URI
from tasks.common import ENV
from tasks.common import load_environ
from tasks.common import print_title


@task
def httpd(c, config_uri=CONFIG_URI, env=ENV):
    """Run the development HTTP server."""

    print_title(httpd.__doc__)

    load_environ(env)

    c.run(f'gunicorn --paste {config_uri} $GUNICORN_OPTS', pty=True)
