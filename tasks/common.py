import os
import subprocess

from termcolor import cprint


CONFIG_URI = 'conf/application.ini'
ENV = 'conf/environment'


def load_environ(filepath):
    """ Load environment variables from file """

    if not filepath:
        return

    output = subprocess.getoutput(f'env -i sh -c "set -a && . {filepath} && env -0"')

    os.environ.update(dict(
        line.split('=', 1)
        for line in output.split('\x00')
        if '=' in line
    ))


def print_title(title):
    cprint(f'\n* {title}', 'blue')
