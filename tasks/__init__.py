from invoke import Collection

import tasks.i18n
import tasks.service
import tasks.test


# Initialize tasks collection
ns = Collection()
ns.add_collection(tasks.i18n)
ns.add_collection(tasks.service)
ns.add_collection(tasks.test)
