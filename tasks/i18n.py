from invoke import task

from tasks.common import print_title

PREFIX = 'pyramid_helpers'
DOMAIN = 'pyramid-helpers'


@task
def compile(c):
    """Compile message catalogs to MO files."""

    print_title(compile.__doc__)

    c.run(
        'pybabel compile '
        '--directory={0}/locale '
        '--domain={1} '
        '--statistics'.format(PREFIX, DOMAIN),
        pty=True
    )


@task
def extract(c):
    """Extract messages from source files and generate a POT file."""

    print_title(extract.__doc__)

    c.run(
        'pybabel extract '
        '-F babel.cfg '
        '--keyword=translate --keyword=pluralize:1,2 '
        '--msgid-bugs-address=BUGS '
        '--copyright-holder=COPYRIGHT_HOLDER '
        '--add-comments=TRANSLATORS: '
        '--output-file={0}/locale/{1}.pot '
        '--width=83 '
        '{0}'.format(PREFIX, DOMAIN),
        pty=True
    )


@task(help={'locale': 'Short name of the locale to generate PO file for.'})
def init(c, locale):
    """Create new message catalogs from a POT file."""

    print_title(init.__doc__)

    c.run(
        'pybabel init '
        '--domain={1} '
        '--input-file={0}/locale/{1}.pot '
        '--output-dir={0}/locale '
        '--locale={2}'.format(PREFIX, DOMAIN, locale),
        pty=True
    )


@task
def to_json(c):
    """Compile JSON from PO file"""

    print_title(to_json.__doc__)

    c.run(
        'po2json {0}/locale/ {0}/static/translations/ {1}'.format(PREFIX, DOMAIN),
        pty=True
    )


@task
def update(c):
    """Update existing message catalogs from a POT file."""

    print_title(update.__doc__)

    c.run(
        'pybabel update '
        '--domain={1} '
        '--input-file={0}/locale/{1}.pot '
        '--output-dir={0}/locale '
        '--previous '
        '--width=83'.format(PREFIX, DOMAIN),
        pty=True
    )


@task(compile, to_json)
def generate(c):
    pass


@task(update, generate, default=True)
def all(c):
    """Run the i18n pipeline."""
    pass
