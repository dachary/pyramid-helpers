<%inherit file="../site.mako" />
<div class="page-header">
    <h2>${_('Articles')}</h2>
</div>
<div class="partial-block" data-partial-key="articles.partial">
<%include file="list.mako" />
</div>
