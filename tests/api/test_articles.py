""" Test functions for api.articles module """

from pytest_steps import test_steps

from robber import expect

# pylint: disable=no-member


def extract_result(response, status_code, status):
    """  Extract result from response """

    expect(response.status_code).to.eq(status_code)

    try:
        result = response.json()
    except ValueError:
        result = None

    expect(result).to.be.a.dict()
    expect(result).to.contain('apiVersion', 'method', 'result')
    expect(result['apiVersion']).to.eq('1.0')

    if status:
        expect(result['result']).to.be.true()
    else:
        expect(result['result']).to.be.false()

    return result


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'duplicate', 'invalid')
def test_create(m_session, g_session, a_session):
    """ Test function for api.articles.create view """

    # Anonymous
    response = m_session.post(f'{m_session.api_url}/articles')

    expect(response.status_code).to.eq(200)
    expect(response.text).to.contain('<form action="/login"')
    yield

    # Guest OK
    data = dict(
        title='Test article #1',
        status='draft',
        text='text',
    )

    response = g_session.post(f'{m_session.api_url}/articles', data=data)

    extract_result(response, 201, True)
    yield

    # Admin OK
    data = dict(
        title='Test article #2',
        status='draft',
        text='text',
    )

    response = a_session.post(f'{m_session.api_url}/articles', data=data)

    extract_result(response, 201, True)
    yield

    # Duplicate
    response = a_session.post(f'{m_session.api_url}/articles', data=data)

    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.match('Title already used in another article')
    yield

    # Invalid
    response = g_session.post(f'{m_session.api_url}/articles', data=dict(text=None))

    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.contain('Missing value')
    yield


@test_steps('normal', 'selected_ids', 'first_page', 'second_page', 'all')
def test_search(m_session):
    """ Test function for api.articles.search view """

    # Normal
    params = {
        'title': 'Test article',
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    article_ids = [article['id'] for article in result['articles']['items']]
    expect(len(article_ids)).to.eq(2)
    yield

    # Selected ids
    params = dict(
        selected_ids=','.join(map(str, article_ids)),
    )

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(len(article_ids))
    yield

    # first page
    params['articles.limit'] = '1'
    params['articles.page'] = '1'

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(result['articles']['pager']['total']).to.eq(2)
    expect(len(result['articles']['items'])).to.eq(1)
    yield

    # second page
    params['articles.page'] = '2'

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(2)
    expect(result['articles']['pager']['total']).to.eq(2)
    expect(len(result['articles']['items'])).to.eq(1)
    yield

    # all
    params['articles.limit'] = '10'

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(result['articles']['pager']['total']).to.eq(2)
    expect(len(result['articles']['items'])).to.eq(2)
    yield


@test_steps('search', 'anonymous', 'guest_ok', 'admin_ok', 'invalid', 'not_found')
def test_modify(m_session, g_session, a_session):
    """ Test function for api.articles.modify view """

    # Search
    params = {
        'title': 'Test article',
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    articles = result['articles']['items']
    expect(len(articles)).to.eq(2)

    article1, article2 = articles

    article1['text'] = 'Modified text #1'
    article1_id = article1['id']

    article2['text'] = 'Modified text #2'
    article2_id = article2['id']
    yield

    # Anonymous
    response = m_session.put(f'{m_session.api_url}/articles/{article1_id}', data=article1)

    expect(response.status_code).to.eq(200)
    expect(response.text).to.contain('<form action="/login"')
    yield

    # Guest OK
    response = g_session.put(f'{m_session.api_url}/articles/{article1_id}', data=article1)

    result = extract_result(response, 200, True)
    yield

    # Admin OK
    response = a_session.put(f'{m_session.api_url}/articles/{article2_id}', data=article2)

    result = extract_result(response, 200, True)
    yield

    # Invalid
    response = a_session.put(f'{m_session.api_url}/articles/{article2_id}', data=dict(title=None))

    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.contain('Missing value')
    yield

    # Not Found
    response = a_session.put(f'{m_session.api_url}/articles/0')

    expect(response.status_code).to.eq(404)
    yield


@test_steps('search', 'anonymous', 'guest_ok', 'admin_ok', 'invalid', 'not_found')
def test_status(m_session, g_session, a_session):
    """ Test function for api.articles.status view """

    # Search
    params = {
        'title': 'Test article',
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    article_ids = [article['id'] for article in result['articles']['items']]
    expect(len(article_ids)).to.eq(2)

    article1_id, article2_id = article_ids
    yield

    # Anonymous
    response = m_session.put(f'{m_session.api_url}/articles/{article1_id}/status', data=dict(status='published'))

    expect(response.status_code).to.eq(200)
    expect(response.text).to.contain('<form action="/login"')
    yield

    # Guest OK
    response = g_session.put(f'{m_session.api_url}/articles/{article1_id}/status', data=dict(status='published'))

    result = extract_result(response, 200, True)
    yield

    # Admin OK
    response = a_session.put(f'{m_session.api_url}/articles/{article2_id}/status', data=dict(status='refused'))

    result = extract_result(response, 200, True)
    yield

    # Invalid
    response = a_session.put(f'{m_session.api_url}/articles/{article2_id}/status', data=dict(status='invalid'))

    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('status')
    expect(result['errors']['status']).to.contain('Value must be one of: draft; published; refused')
    yield

    # Not Found
    response = a_session.put(f'{m_session.api_url}/articles/0/status')

    expect(response.status_code).to.eq(404)
    yield


@test_steps('search', 'article1', 'article2', 'not_found')
def test_visual(m_session):
    """ Test function for api.articles.visual view """

    # Search
    params = {
        'title': 'Test article',
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    article_ids = [article['id'] for article in result['articles']['items']]
    expect(len(article_ids)).to.eq(2)

    article1_id, article2_id = article_ids
    yield

    # Article #1
    response = m_session.get(f'{m_session.api_url}/articles/{article1_id}')

    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('Modified text #1')
    expect(result['article']['status']).to.eq('published')
    yield

    # Article #2
    response = m_session.get(f'{m_session.api_url}/articles/{article2_id}')

    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('Modified text #2')
    expect(result['article']['status']).to.eq('refused')
    yield

    # Not Found
    response = m_session.get(f'{m_session.api_url}/articles/0')

    expect(response.status_code).to.eq(404)
    yield


@test_steps('search', 'anonymous', 'guest', 'admin_ok', 'not_found')
def test_delete(m_session, g_session, a_session):
    """ Test function for api.articles.delete view """

    # Search
    params = {
        'title': 'Test article',
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    response = m_session.get(f'{m_session.api_url}/articles', params=params)

    result = extract_result(response, 200, True)

    article_ids = [article['id'] for article in result['articles']['items']]
    expect(len(article_ids)).to.eq(2)
    yield

    # Anonymous
    response = m_session.delete(f'{m_session.api_url}/articles/{article_ids[0]}')

    expect(response.status_code).to.eq(200)
    expect(response.text).to.contain('<form action="/login"')
    yield

    # Guest
    response = g_session.delete(f'{m_session.api_url}/articles/{article_ids[0]}')

    expect(response.status_code).to.eq(403)
    yield

    # Admin OK
    for article_id in article_ids:
        response = a_session.delete(f'{m_session.api_url}/articles/{article_id}')

        result = extract_result(response, 200, True)
    yield

    # Not Found
    response = a_session.delete(f'{m_session.api_url}/articles/0')

    expect(response.status_code).to.eq(404)
    yield
