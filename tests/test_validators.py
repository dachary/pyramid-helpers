""" Test functions for forms.validators module """

import datetime

import formencode
from formencode import validators

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.forms.validators import Color
from pyramid_helpers.forms.validators import DateTime
from pyramid_helpers.forms.validators import List
from pyramid_helpers.forms.validators import Month
from pyramid_helpers.forms.validators import Time
from pyramid_helpers.forms.validators import Week


# pylint: disable=no-member

@test_steps('to_hex', 'from_hex', 'from_int', 'invalid', 'empty')
def test_color_hex():
    """ Test function for forms.validators.Color with hex strings """

    color_hex = '#aabbcc'
    color_int = [170, 187, 204]

    validator = Color(format='hex')

    expect(validator.to_python(color_hex)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_hex)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_int)).to.eq(color_hex)
    yield

    expect(lambda: validator.to_python('invalid')).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('to_hex', 'from_hex', 'from_int', 'invalid', 'empty')
def test_color_int():
    """ Test function for forms.validators.Color with integers """

    color_hex = '#aabbcc'
    color_int = [170, 187, 204]

    validator = Color(format='int')

    expect(validator.to_python(color_hex)).to.eq(color_int)
    yield

    expect(validator.from_python(color_hex)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_int)).to.eq(color_hex)
    yield

    expect(lambda: validator.to_python('invalid')).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('to', 'from', 'to_invalid', 'from_invalid', 'empty')
def test_date():
    """ Test function for forms.validators.DateTime with date objects """

    value_str = '12/25/2020'
    value_inv = '25/12/2020'
    value_obj = datetime.date(2020, 12, 25)

    validator = DateTime(format='%m/%d/%Y', is_date=True, is_localized=False)

    expect(validator.to_python(value_str)).to.eq(value_obj)
    yield

    expect(validator.from_python(value_obj)).to.eq(value_str)
    yield

    expect(lambda: validator.to_python(value_inv)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.from_python(value_inv)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('to', 'from', 'to_invalid', 'from_invalid', 'empty')
def test_datetime():
    """ Test function for forms.validators.DateTime with datetime objects """

    value_str = '12/25/2020 12:52:32'
    value_inv = '12/25/2020 25:52:32'
    value_obj = datetime.datetime(2020, 12, 25, 12, 52, 32)

    validator = DateTime(format='%m/%d/%Y %H:%M:%S', is_date=False, is_localized=False)

    expect(validator.to_python(value_str)).to.eq(value_obj)
    yield

    expect(validator.from_python(value_obj)).to.eq(value_str)
    yield

    expect(lambda: validator.to_python(value_inv)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.from_python(value_inv)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('int_to', 'int_from', 'int_invalid', 'str_to', 'str_to2', 'str_from', 'empty')
def test_list():
    """ Test function for forms.validators.List """

    # List of integers
    validator = List(validators.Int())

    expect(validator.to_python('1, 2, 3')).to.eq([1, 2, 3])
    yield

    expect(validator.from_python([1, 2, 3])).to.eq('1,2,3')
    yield

    expect(lambda: validator.to_python('::')).to.throw(formencode.Invalid)
    yield

    # List of strings
    validator = List()

    expect(validator.to_python('a, b, c')).to.eq(['a', 'b', 'c'])
    yield

    expect(validator.to_python('::')).to.eq(['::'])
    yield

    expect(validator.from_python(['a', 'b', 'c'])).to.eq('a,b,c')
    yield

    expect(validator.to_python(None)).to.eq([])
    yield


@test_steps('valid_to', 'valid_from', 'year_min', 'month_min', 'month_max', 'empty')
def test_month():
    """ Test function for forms.validators.Month """

    validator = Month()

    expect(validator.to_python('1900-01')).to.eq((1900, 1))
    expect(validator.to_python('2020-01')).to.eq((2020, 1))
    expect(validator.to_python('2020-12')).to.eq((2020, 12))
    yield

    expect(validator.from_python((1900, 1))).to.eq('1900-01')
    expect(validator.from_python((2020, 1))).to.eq('2020-01')
    expect(validator.from_python((2020, 12))).to.eq('2020-12')
    yield

    expect(lambda: validator.to_python('1899-01')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-00')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-13')).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('valid_to', 'valid_from', 'hours_min', 'hours_max', 'minutes_min', 'minutes_max', 'empty')
def test_time():
    """ Test function for forms.validators.Time """

    validator = Time()

    expect(validator.to_python('00:00')).to.eq((0, 0))
    expect(validator.to_python('11:11')).to.eq((11, 11))
    expect(validator.to_python('23:59')).to.eq((23, 59))
    yield

    expect(validator.from_python((0, 0))).to.eq('00:00')
    expect(validator.from_python((11, 11))).to.eq('11:11')
    expect(validator.from_python((23, 59))).to.eq('23:59')
    yield

    expect(lambda: validator.to_python('-1:00')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('24:00')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('00:-1')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('00:60')).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield


@test_steps('valid_to', 'valid_from', 'year_min', 'week_min', 'week_max', 'empty')
def test_week():
    """ Test function for forms.validators.Week """

    validator = Week()

    expect(validator.to_python('1900-W01')).to.eq((1900, 1))
    expect(validator.to_python('2020-W01')).to.eq((2020, 1))
    expect(validator.to_python('2020-W52')).to.eq((2020, 52))
    yield

    expect(validator.from_python((1900, 1))).to.eq('1900-W01')
    expect(validator.from_python((2020, 1))).to.eq('2020-W01')
    expect(validator.from_python((2020, 52))).to.eq('2020-W52')
    yield

    expect(lambda: validator.to_python('1899-W01')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-W00')).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-W53')).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None)).to.be.none()
    yield
