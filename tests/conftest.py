""" Tests functions for Pyramid-Helpers """

import pytest
import requests
from webtest import TestApp

from pyramid.paster import get_appsettings

from pyramid_helpers import main

# pylint: disable=unused-argument

APP_URL = 'https://0.0.0.0:6543'
API_URL = f'{APP_URL}/api/1.0'


def pytest_addoption(parser):
    """ Add custom options to parser """

    parser.addoption(
        '--config-uri',
        default='conf/application.ini',
        help='Configuration file'
    )


@pytest.fixture()
def app(request):
    """ Initialize Pyramid-Helper application """

    config_uri = request.config.getoption('--config-uri')

    settings = get_appsettings(config_uri, name='main')

    app_ = main({}, **settings)

    yield TestApp(app_)


@pytest.fixture(scope='session')
def m_session(request):
    """ Initialize main anonymous session """

    session = get_session()

    yield session


@pytest.fixture(scope='session')
def a_session(request):
    """ Initialize an admin session """

    session = get_session()

    do_login(session, 'admin', 'admin')

    yield session

    do_logout(session)


@pytest.fixture(scope='session')
def g_session(request):
    """ Initialize a guest session """

    session = get_session()

    do_login(session, 'guest', 'guest')

    yield session

    do_logout(session)


def get_session():
    """ Initialize a requests session object """

    session = requests.Session()

    # Store APP urls
    session.app_url = APP_URL
    session.api_url = API_URL

    # Disable SSL verification
    session.verify = False

    return session


def do_login(session, username, password):
    """ Log in tp application """

    # Perform login
    data = dict(
        came_from='/',
        username=username,
        password=password,
    )

    session.post(f'{APP_URL}/login', data=data)


def do_logout(session):
    """ Log out from application """

    session.get(f'{APP_URL}/logout')
